#!/usr/bin/env sh

set -ex

[ -e ~/CONDA_ACTIVATE ] && . ~/CONDA_ACTIVATE

mkdir -p generated
cd generated
jupytext --to notebook --output julia_clean.ipynb --set-kernel julia-1.2 ../notebook/julia.jl
jupyter nbconvert --execute --ExecutePreprocessor.timeout=-1 julia_clean.ipynb --output-dir=. --output=julia.ipynb --to notebook
jupyter nbconvert julia.ipynb --output-dir=. --to html
jupyter nbconvert julia.ipynb --output-dir=. --to slides --SlidesExporter.reveal_scroll=True
