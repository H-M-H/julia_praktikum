#!/usr/bin/env sh

cd notebook
for nb in *.ipynb
do
    jupytext --to jl:percent --update-metadata '{"jupytext": {"notebook_metadata_filter":"-all"}}' "$nb"
done

