# -*- coding: utf-8 -*-
# %% [markdown] {"slideshow": {"slide_type": "slide"}}
# # Julia fürs Praktikum
#
# ## Link:
# - https://h-m-h.gitlab.io/julia_praktikum/

# %% [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Aufbau
# - Warum Julia?
#   - Warum programmieren?
#   - Alternativen
# - Bibiliotheken/Pakete installieren
# - Jupyter und Notebooks
# - Julia: Grundlagen
# - Daten einlesen und speichern (CSV, DataFrames)
# - Mit DataFrames rechnen
# - Daten fitten
# - Fehlerrechnung mit Measurements.jl
# - Plots
#   - Plots erstellen
#   - Plots speichern

# %% [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Warum Julia?
# - Julia ist eine Programmiersprache, die insbesondere auf wissenschaftliches Rechnen ausgelegt ist.
#   - Einfach zu programmieren
#   - Schnell für große Berechnungen
#   - Viele Bibliotheken für Datenauswertung, Plotten, ...
#
# ### Warum programmieren?
# - Daten und Rechnung sind getrennt und Auswertung lässt sich besser Nachvollziehen.
# - Neue Daten lassen sich trivial auswerten.
# - Fehler in irgendeinem Auswertungsschritt lassen sich trivial korrigieren.

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# ### Alternativen
# - Python (3)
#   - Sehr weit verbreitet, größere Nutzerbasis
#   - Mehr und länger erprobte Bibliotheken
#   - Datenauswertung über numpy umständlicher
#   - Python selbst ist langsam
#
#  
# - R & RStudio
#   - Speziell ausgelegt auf Datenauswertung
#   - Viele gut gepflegte Bibliotheken wie z. B. ggplot2
#   - Syntax etwas gewöhnungsbedürftig
#   - R selbst ist langsam

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# ## Bibliotheken/Pakete installieren und benutzen
# - Fast alle Programmiersprachen leben davon, dass (Software-)Bibliotheken installiert werden können, die zusätzliche Funktionalitäten enthalten.
#   - So muss nicht alles selbst programmiert werden.
# - In Julia geht das wie folgt:
#
# ### Installation:
# ```jlcon
# julia> ]
# (v1.2) pkg> add Plots
# Updating registry at `~/.julia/registries/General`
# ...
# ```

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# #### Benutzen:
# ```jlcon
# julia> a = [0, 84]
# 2-element Array{Int64,1}:
#   0
#  84
#
# julia> mean(a)
# ERROR: UndefVarError: mean not defined
# Stacktrace:
#  [1] top-level scope at REPL[2]:1
#
# julia> using Statistics
#
# julia> mean(a)
# 42.0
# ```

# %% [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Jupyter und Notebooks
# - Diese Präsentation ist ein Jupyter Notebook.
# - Code wird in Zellen unterteilt, die einzeln ausgeführt werden können.
# - Julia wird nur einmal gestartet und läuft die gesamte Zeit im Hintergrund.
#
# ### Alternativen
# - Atom mit [Juno](https://junolab.org/) oder [Hydrogen](https://nteract.gitbooks.io/hydrogen/)
# - VSCode

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# ### Notebook starten
# Als normalen Notebookserver:
# ```jlcon
# julia> using IJulia
# julia> notebook(dir="irgend/ein/ordner");
# ```
#
# Über Jupyterlab:
# ```jlcon
# julia> using IJulia
# julia> jupyterlab(dir="irgend/ein/ordner");
# ```

# %% [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Julia: Grundlagen
# - Erstmal: Hilfe zur Selbsthilfe
#   - Offizielle Dokumentation von Julia: https://docs.julialang.org
#   - Fehlermeldungen sind Freunde, keine Feinde
#   - Programmierer sind nur Menschen, die gut googeln können --> stackoverflow

# %% [markdown] {"slideshow": {"slide_type": "slide"}}
# ### Grundlagen der Sprache

# %% [markdown] {"slideshow": {"slide_type": "fragment"}}
# #### Datentypen und Variablen

# %% {"slideshow": {"slide_type": "-"}}
1 + 2

# %% {"slideshow": {"slide_type": "-"}}
x = 1 + 2

# %% {"slideshow": {"slide_type": "-"}}
s = "Versuche"

# %% {"slideshow": {"slide_type": "-"}}
satz = "In $x Semestern kann ich mindestens $(10 * x) $s machen!"

# %% {"slideshow": {"slide_type": "subslide"}}
eine_liste = [1.5, 4.3, 17.34, -124.9, 44.3]

# %% {"slideshow": {"slide_type": "-"}}
eine_liste[1]

# %% {"slideshow": {"slide_type": "-"}}
eine_liste[5]

# %% {"slideshow": {"slide_type": "-"}}
eine_liste[end]

# %% {"slideshow": {"slide_type": "subslide"}}
eine_liste[end-1]

# %% {"slideshow": {"slide_type": "-"}}
eine_liste[4] = -25.44
eine_liste

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# #### Funktionen

# %% [markdown] {"slideshow": {"slide_type": "fragment"}}
# - Funktionen können, ähnlich wie in der Mathematik von Eingabewerten/Parametern abhängen, müssen das aber nicht.
# - Funktionen können etwas zum weiterbenutzen zurückgeben, müssen das aber nicht.
# - Funktionen sind nichts anderes als ein Stück Code, der als Block zusammengefasst ist und über den Funktionsnamen aufgerufen werden kann.
# - Es gibt viele vordefinierte Funktionen.

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# ##### Beispiele

# %% {"slideshow": {"slide_type": "-"}}
uppercase(satz)

# %% {"slideshow": {"slide_type": "-"}}
sort!(eine_liste)

# %% {"slideshow": {"slide_type": "subslide"}}
sum(eine_liste)

# %% {"slideshow": {"slide_type": "-"}}
push!(eine_liste, 13)

# %% [markdown] {"slideshow": {"slide_type": "subslide"}}
# ##### Eigene Funktionen
# - Funktionen können auf folgende Weise definiert werden:

# %% {"slideshow": {"slide_type": "fragment"}}
# Wie in der Mathematik

eine_norm(x, y) = √(x^2 + y^2)  # √ bekommt man durch eingabe von \sqrt und dann Tab drücken.
eine_norm(3, 4)

# %% {"slideshow": {"slide_type": "fragment"}}
# Oder für längere Funktionen mit

function interpolation(x0, x1, y0, y1, x)
    m = (y1 - y0) / (x1 - x0)
    b = y0 - m * x0
    return m * x + b
end

interpolation(0, 10, 8, 0, 5)

# %% {"slideshow": {"slide_type": "slide"}}
using
    CSV,
    DataFrames,
    Plots,
    LaTeXStrings,
    LsqFit

pyplot()

# %% {"slideshow": {"slide_type": "fragment"}}
@. f(x, p) = p[1] * exp(x/p[2])

x = 1:100
y = f(x, [5, 20]) .+ (rand(length(x)) .- 0.5) .* 50

p0 = [4.0, 25.0]
fit = curve_fit(f, x, y, p0)

Δp1, Δp2 = round.(stderror(fit), digits=2)

p1, p2 = round.(fit.param, digits=2)
plot(x, y, title="\$f(x) = ($p1 \\pm $Δp1) \\cdot \\exp\\left(\\frac{x}{$p2 \\pm $Δp2}\\right)\$")
plot!(x->f(x, fit.param))

# %%
