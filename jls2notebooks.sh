
#!/usr/bin/env sh

cd notebook
for jl in *.jl
do
    jupytext --to notebook --output julia.ipynb --set-kernel julia-1.2 julia.jl
done

