#!/usr/bin/env sh

set -ex

mkdir -p public

cp generated/* public
